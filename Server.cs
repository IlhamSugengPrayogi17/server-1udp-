using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class Server
{
    public static void Main()
    {
        string message = " ";
        int recv;
        byte[] data = new byte[1024];
        IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 12000);

        Socket newsock = new Socket(AddressFamily.InterNetwork,
                        SocketType.Dgram, ProtocolType.Udp);

        newsock.Bind(ipep);
        Console.WriteLine("Waiting");

        IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
        EndPoint tmpRemote = (EndPoint)(sender);

        recv = newsock.ReceiveFrom(data, ref tmpRemote);

        Console.WriteLine("Pesan Diterima:", tmpRemote.ToString());
        Console.WriteLine(Encoding.ASCII.GetString(data, 0, recv) + "login");

        string welcome = "Welcome";
        data = Encoding.ASCII.GetBytes(welcome);
        newsock.SendTo(data, data.Length, SocketFlags.None, tmpRemote);
        
        while (true)
        {
            data = new byte[1024];
            recv = newsock.ReceiveFrom(data, ref tmpRemote);
            Console.WriteLine("Klien : " + Encoding.ASCII.GetString(data, 0, recv));

            Console.Write("message : ");
            message = Console.ReadLine();
            data = Encoding.ASCII.GetBytes(message);
            newsock.SendTo(data, data.Length, SocketFlags.None, tmpRemote);

        }
        newsock.Close();
    }
}
